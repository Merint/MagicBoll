//
//  ViewController.swift
//  MagicBoll
//
//  Created by Merin K Thomas on 02/12/22.
//

import UIKit

class ViewController: UIViewController {

    let ballArray = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    var randomBallNumber = 0
    @IBOutlet weak var ImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newBall()
        
    }
 
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        newBall()
    }
    @IBAction func askButton(_ sender: UIButton) {
        newBall()
    }
    
    func newBall() {
        randomBallNumber = Int(arc4random_uniform(5))
        ImageView.image = UIImage(named: ballArray[randomBallNumber])
    }
    
    
}

